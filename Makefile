CXX=g++

CXXFLAGS=-Wall -Werror -Wextra -std=c++11 -pedantic -O3

DEBUGFLAGS=-DDEBUG

SRC=$(addprefix src/, main.cc $(addprefix dice/, dice.cc) $(addprefix sector/, \
		sector.cc planet.cc) $(addprefix faction/, faction.cc))

OBJS=${SRC:.cc=.o}

TEST=check

OUT=blackcrusade

all:${OBJS}
	${CXX} ${CXXFLAGS} -o ${OUT} ${SRC}

debug:${OBJS}
	${CXX} ${CXXFLAGS} ${DEBUGFLAGS} -o ${OUT} ${SRC}

${TEST}: ${OBJS}
	${CXX} ${CXXFLAGS} -o ${OUT} ${SRC}
	./${OUT} < ${TEST}/test

clean:
	rm -f ${OBJS} ${OUT}

distclean:clean
	rm -f ${RULES}

.PHONY: clean check
