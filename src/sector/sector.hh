#pragma once

#include <vector>
#include "planet.hh"

class Sector
{
    public:
        Sector();
        std::vector<Planet> getVplanets() const;
        void parse(std::string file);
        void play_turn();
        void print(std::string file) const;

    private:

        std::vector<Planet> vplanets;
};
