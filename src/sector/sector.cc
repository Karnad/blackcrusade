#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include "sector.hh"

Sector::Sector()
    : vplanets()
{}

std::vector<Planet> Sector::getVplanets() const
{
    return vplanets;
}

void Sector::parse(std::string file)
{
    std::ifstream i_file(file);
    std::string line;

    std::getline(i_file, line);

    while (std::getline(i_file, line))
    {
        std::replace(line.begin(), line.end(), ',', ' ');
        #if DEBUG
        std::cout << line << std::endl;
        #endif
        std::stringstream ss(line);
        std::istream_iterator<std::string> begin(ss);
        std::istream_iterator<std::string> end;
        std::vector<std::string> vargs(begin, end);
        vplanets.push_back(Planet(vargs));
    }
}

void Sector::play_turn()
{
    std::for_each(vplanets.begin(), vplanets.end(),
            [](Planet& planet){planet.playTurn();});
}

void Sector::print(std::string file) const
{
    file.replace(file.find(".csv", 0), 4, "");
    file.replace(6, file.size()-6, std::to_string(stoi(file.substr(6,file.size()-6)) + 1));
    std::ofstream o_file(file + ".csv");
    o_file << "Planet,Owner,Naval Strength,Defender Strength,Corrupted,Plunder Points,Deathwatched" << std::endl;
    for (const auto& it : vplanets)
        it.print(o_file);
    
    o_file.close();
}
