#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "../faction/faction.hh"

#define RESET   "\033[0m"
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */

class Planet
{
    public:
        Planet(const std::vector<std::string>& args);
        std::string getName() const;
        Faction getOwner() const;
        int getPlunderPoints() const;
        bool isPlayerControlled() const;
        void increasePlunderPoints(int diff);
        void decreasePlunderPoints(int diff);
        void playTurn();
        void print(std::ofstream &o_file) const;

    private:
        std::string name; 
        Faction owner;
        int n_strength;
        int d_strength;
        bool is_corrupted;
        int plunder_points;
        bool is_deathwatched;
        
        std::string boolToStr(bool boule) const;
        void generatePlunder();
        void generateReinforcement();
};
