#include <algorithm>
#include <cstdlib>
#include "../dice/dice.hh"
#include "planet.hh"

Planet::Planet(const std::vector<std::string>& args)
    : name(args[0])
    , owner(args[1])
    , n_strength(std::atoi(args[2].c_str()))
    , d_strength(std::atoi(args[3].c_str()))
    , is_corrupted(!args[4].compare("Oui"))
    , plunder_points(std::atoi(args[5].c_str()))
    , is_deathwatched(!args[6].compare("Oui"))
{}

std::string Planet::getName() const
{
    return name;
}

Faction Planet::getOwner() const
{
    return owner;
}

int Planet::getPlunderPoints() const
{
    return plunder_points;
}

bool Planet::isPlayerControlled() const
{
    return owner.isPlayerControlled();
}

void Planet::increasePlunderPoints(int diff)
{
    plunder_points += diff;
}
 
void Planet::decreasePlunderPoints(int diff)
{
    plunder_points -= diff;
}
       
void Planet::playTurn()
{
   generatePlunder(); 
   generateReinforcement();
}

void Planet::print(std::ofstream &o_file) const
{
    o_file << name << "," << owner.getName() << "," << n_strength << "," <<
        d_strength << "," << boolToStr(is_corrupted) << "," <<
        plunder_points << "," << boolToStr(is_deathwatched) << std::endl;
}

std::string Planet::boolToStr(bool boule) const
{
    if (boule)
        return "Oui";
    else
        return "Non";
}

void Planet::generatePlunder()
{
    if (is_corrupted)
    {
        std::cout << name << " generated plunder points" << std::endl;
        if (owner.getId() == TYRANID)
            increasePlunderPoints(1);
        else
            increasePlunderPoints(2);
    }
}

void Planet::generateReinforcement()
{
    if (owner.isPlayerControlled())
    {
        Dice dice(10);
        int result = dice.roll();
        if (is_corrupted)
            result--;
        if (result >= 7)
        {
            d_strength += dice.roll();
            switch (owner.getId())
            {
                case IMPERIUM:
                    std::cout << YELLOW;
                    is_deathwatched = (dice.roll() >= 8);
                    break;
                case CHAOS:
                    std::cout << RED;
                    break;
                case TYRANID:
                    std::cout << MAGENTA;
                    break;
                case TAU:
                    std::cout << CYAN;
                    break;
            };
            std::cout << name << " has received reinforcements" << std::endl;
            if (is_deathwatched)
                std::cout << BOLDYELLOW << "Deathwatch on " << name << std::endl;
            std::cout << RESET;
        }
    }
}
