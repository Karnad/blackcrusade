#pragma once
#include <random>

class Dice
{
    public:
        Dice(int numberOfFaces);

        int roll();

    private:
        std::random_device rd;
        std::mt19937 mt;
        std::uniform_int_distribution<> distribution;

};
