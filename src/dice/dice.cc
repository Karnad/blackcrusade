#include "dice.hh"

Dice::Dice(int numberOfFaces)
    : rd()
    , mt(rd())
    , distribution(1, numberOfFaces)
{}

int Dice::roll()
{
    return distribution(mt);
}
