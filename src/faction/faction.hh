#pragma once

#include <string> 
#include <utility>
#include <vector>

#define IMPERIUM 1
#define CHAOS 2
#define TYRANID 3
#define TAU 4
#define ORK 5
#define NECRON 6

class Faction
{
    public:
        Faction(const std::string& factionName);
        bool operator==(const Faction& faction) const;
        bool operator==(const int factionId) const;
        std::string getName() const;
        bool isPlayerControlled() const;
        int getId() const;

    private:
        static std::vector<std::pair<std::string, bool>> vfactions;
        int id;
};
