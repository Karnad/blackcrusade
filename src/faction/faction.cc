#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include "faction.hh"

std::vector<std::pair<std::string, bool>> Faction::vfactions = [] {
    std::ifstream i_file("conf/factions");
    std::string line;
    std::vector<std::pair<std::string, bool>> vfactions;
    int count = 0;

    while (std::getline(i_file, line))
    {
        std::stringstream ss(line);
        std::istream_iterator<std::string> begin(ss);
        std::istream_iterator<std::string> end;
        std::vector<std::string> vargs(begin, end);
        bool isPlayer = !(count++ == 1);
        for (const auto& it : vargs)
            vfactions.push_back({it, isPlayer});
    }

    return vfactions;
}();

Faction::Faction(const std::string& factionName)
    :id(std::find_if(vfactions.begin(), vfactions.end(),
                [&factionName](const std::pair<std::string, bool>& element){
                return !element.first.compare(factionName);}) - vfactions.begin())
{}

bool Faction::operator==(const Faction& faction) const
{
    return id == faction.id;
}

bool Faction::operator==(const int factionId) const
{
    return id == factionId;
}

bool Faction::isPlayerControlled() const
{
    return vfactions.at(id).second;
}

std::string Faction::getName() const
{
    return vfactions.at(id).first;
}

int Faction::getId() const
{
    return id;
}
