#include <iostream>
#include "sector/sector.hh"

int main(int argc, const char* argv[])
{
    std::string options = "-h";
    if (argc > 0 && !options.compare(argv[1]))
        std::cout << "blackcrusade [nameoftheinputfile]" << std::endl;
    Sector map;
    map.parse(argv[1]);
    #if DEBUG
    for (const auto& it : map.getVplanets())
        std::cout << it.getName() << std::endl;
    #endif
    map.play_turn();
    map.print(argv[1]);
}
